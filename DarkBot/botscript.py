#from __future__ import print_function
import time
from pololu_drv8835_rpi import motors, MAX_SPEED
import RPi.GPIO as GPIO
import json
#import gps
#import threading
#import sys

interface_file=open('com/interface.json','r')

#setup

# GPS listens on port 2947 (gpsd) of localhost
#session = gps.gps("localhost", "2947")
#session.stream(gps.WATCH_ENABLE | gps.WATCH_NEWSTYLE)

left_=0

right_=0

pan_pin=36
pan_min=5
pan_max=10
pan_=0

tilt_pin=35
tilt_min=5
tilt_max=10
tilt_=0

light_pin=37
light_min=0
light_max=100
light_=0

ir_pin=38
ir_min=0
ir_max=100
ir_=0

GPIO.setmode(GPIO.BOARD)
GPIO.setwarnings(False)
GPIO.setup(pan_pin, GPIO.OUT)
GPIO.setup(tilt_pin, GPIO.OUT)
GPIO.setup(light_pin, GPIO.OUT)
GPIO.setup(ir_pin, GPIO.OUT)

pan = GPIO.PWM(pan_pin,50)
pan_=''
tilt = GPIO.PWM(tilt_pin,50)
tilt_=''
light = GPIO.PWM(light_pin,50)
light_=''
ir = GPIO.PWM(ir_pin,50)
ir_ = ''
pan.start(7.5)
pan = ''
tilt.start(5)
tilt_ = ''
light.start(0)
light_ = ''
ir.start(0)
ir_ = ''

interface=json.loads(interface_file.read())

# command = ''
# input = threading.Event()
# 
# #read json from stdin
# def keep_command():
#     global command, input
#     for line in sys.stdin:
#         command = line
#         try:
#             interface=json.loads(command)
#             if hasattr(interface,"left"):
#                 left_=interface[u"pan"]
#                 motors.motor1.setSpeed(MAX_SPEED*left_)
#             if hasattr(interface,"right"):
#                 right_=interface[u"right"]
#                 motors.motor2.setSpeed(MAX_SPEED*right_)
#             if hasattr(interface,"pan"):
#                 pan_=interface[u"pan"]
#                 pan.ChangeDutyCycle(float(pan_+1)/2*(pan_max-pan_min)+pan_min)
#             if hasattr(interface,"tilt"):
#                 tilt_=interface[u"tilt"]
#                 tilt.ChangeDutyCycle(tilt_*(tilt_max-tilt_min)+tilt_min)
#             if hasattr(interface,"light"):
#                 light_=interface[u"light"]
#                 light.ChangeDutyCycle(light_)
#             if hasattr(interface,"ir"):
#                 ir_=interface[u"light"]
#                 ir.ChangeDutyCycle(ir_)
#         except:
#             pass
#         input.set()
# 
# keep_command_thread = threading.Thread(target=keep_command)
# keep_command_thread.daemon = True
# keep_command_thread.start()

try:
    while True:
        interface_file.seek(0)
        if light_!=interface[u"left"]:
            light_=interface[u"left"]
            light.ChangeDutyCycle(left_)
        if light_!=interface[u"right"]:
            light_=interface[u"right"]
            light.ChangeDutyCycle(right_)
        if light_!=interface[u"pan"]:
            light_=interface[u"pan"]
            light.ChangeDutyCycle(pan_)
        if light_!=interface[u"tilt"]:
            light_=interface[u"tilt"]
            light.ChangeDutyCycle(tilt_)
        if light_!=interface[u"light"]:
            light_=interface[u"light"]
            light.ChangeDutyCycle(light_)
        if ir_!=interface[u"ir"]:
            ir_=interface[u"light"]
            ir.ChangeDutyCycle(ir_)
        time.sleep(.1)
#        try:
#Write GPS data to file
#            try:
#                #print 'test'
#                report = session.next()
#		print report['raw']
		#for i in range(len(report.keys())):
		#    print report.keys()[i]
		#print report['json']
                
                #with open('com/gps.json', 'w') as outfile:
                    #json.dump(report, outfile)
                # Wait for a 'TPV' report and display the current time
                # To see all report data, uncomment the line below
                # print report
#                 if report['class'] == 'TPV':
#                     if hasattr(report, 'speed'):
#                         print 'test'
#                         report.speed = report.speed * gps.MPS_TO_KPH
#                         print report.speed
#                     with open('com/gps.json', 'w') as outfile:
#                         json.dump(report, outfile)
#             except KeyError:
#                 pass
#             except KeyboardInterrupt:
#                     quit()
#             except StopIteration:
#                 session = None
#                 time.sleep(.1)
#         except ValueError:
#             time.sleep(.1)
except KeyboardInterrupt:

    pan.stop()
    tilt.stop()
    light.stop()
    ir.stop()
    motors.setSpeeds(0, 0)
    GPIO.cleanup()
