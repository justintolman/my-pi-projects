
import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)
GPIO.setup(36, GPIO.OUT)

pan = GPIO.PWM(36,20)
pan.start(7.5)

try:

    while True:

        pan.ChangeDutyCycle(7.5)

        time.sleep(0.5)

        #pan.ChangeDutyCycle(10.5)    #sends a 10.5% pulse to turn the servo CW

        pan.ChangeDutyCycle(7.5)    #sends a 10.5% pulse to turn the servo CW

        time.sleep(0.5)                   #continues for a half a second

       pan.ChangeDutyCycle(7.5)    #sends a 7.5% pulse to center the servo again

        time.sleep(0.5)                   #continues for a half a second

except KeyboardInterrupt:

    p.stop()

    GPIO.cleanup() 