from __future__ import print_function
import time
from pololu_drv8835_rpi import motors, MAX_SPEED
import RPi.GPIO as GPIO
import json


#setup
#start camera feed

interface_file=open('com/interface.json','r')

left_=0

right_=0

pan_pin=36
pan_min=5
pan_max=10
pan_=0

tilt_pin=35
tilt_min=5
tilt_max=10
tilt_=0

light_pin=37
light_min=0
light_max=100
light_=0

ir_pin=38
ir_min=0
ir_max=100
ir_=0

GPIO.setmode(GPIO.BOARD)
GPIO.setup(pan_pin, GPIO.OUT)
GPIO.setup(tilt_pin, GPIO.OUT)
GPIO.setup(light_pin, GPIO.OUT)
GPIO.setup(ir_pin, GPIO.OUT)

pan = GPIO.PWM(pan_pin,50)
tilt = GPIO.PWM(tilt_pin,50)
light = GPIO.PWM(light_pin,50)
ir = GPIO.PWM(ir_pin,50)
pan.start(7.5)
tilt.start(5)
light.start(0)
ir.start(0)

try:

    while True:
        try:
            interface=json.loads(interface_file.read())
            interface_file.seek(0)
            if left_!=interface[u"left"]:
                left_=interface[u"pan"]
            if right_!=interface[u"right"]:
                right_=interface[u"right"]
            if pan_!=interface[u"pan"]:
                pan_=interface[u"pan"]
                pan.ChangeDutyCycle(float(pan_+1)/2*(pan_max-pan_min)+pan_min)
            if tilt_!=interface[u"tilt"]:
                tilt_=interface[u"tilt"]
                tilt.ChangeDutyCycle(tilt_*(tilt_max-tilt_min)+tilt_min)
            if light_!=interface[u"light"]:
                light_=interface[u"light"]
                light.ChangeDutyCycle(light_)
            if ir_!=interface[u"ir"]:
                ir_=interface[u"light"]
                ir.ChangeDutyCycle(ir_)
            time.sleep(.1)
        except ValueError:
            time.sleep(.1)
except KeyboardInterrupt:

    pan.stop()
    tilt.stop()
    light.stop()
    ir.stop()
    motors.setSpeeds(0, 0)
    GPIO.cleanup() 